package net.chroonus.commands.teamchats;

import net.chroonus.chat.ChatMessages;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamLeaderLocalChatCommand extends Command
{

    private final static String PERMISSION = Permissions.TL_LOCAL.toString();

    public TeamLeaderLocalChatCommand()
    {
        super("tll", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 0)
                {
                    StringBuilder message = new StringBuilder();

                    for (int i = 0; i < args.length; i++)
                    {
                        message.append(args[i]);
                    }

                    ChatMessages.sendLocalTeamMessage(player, PERMISSION, Prefixes.TL_LOCAL, message.toString());

                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/tll §7[§cNachricht§7]"));
                }
            }


        }
    }

}
