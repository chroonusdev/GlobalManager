package net.chroonus.commands.server;

import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HubCommand extends Command
{
    private static final String PERMISSION = Permissions.HUB.toString();

    public HubCommand()
    {
        super("hub", PERMISSION, "l", "lobby", "hubschrauber");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (sender.hasPermission(PERMISSION))
            {
                ServerInfo lobbyServer = ProxyServer.getInstance().getServerInfo("lobby");

                if (player.getServer().getInfo().equals(lobbyServer))
                {
                    player.sendMessage(new TextComponent(Prefixes.ERROR + "Du befindest dich bereits auf der §7Lobby§c!"));
                } else
                {
                    player.connect(lobbyServer);
                    player.sendMessage(new TextComponent(Prefixes.SERVER + "Du wurdest auf die §eLobby §7gesendet!"));
                }
            }
        }
    }
}
