package net.chroonus.commands.server;

import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GoToCommand extends Command
{
    private static final String PERMISSION = Permissions.GOTO.toString();

    public GoToCommand()
    {
        super("goto", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String targetName = args[0];

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(targetName);

                    if (target == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §e" + targetName + " §cist nicht online."));

                    } else if (player.getName().equals(target.getName()))
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "§7Du kannst dir §cnicht §7selbst hinterherspringen."));

                    } else
                    {
                        ServerInfo serverInfo = target.getServer().getInfo();
                        player.connect(serverInfo);

                        player.sendMessage(
                                new TextComponent(Prefixes.SERVER + "Du springst zu " + target.getDisplayName() + " §7auf den Server §e" + serverInfo.getName() + "§7.")
                        );
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/goto §7[§cName§7]"));
                }
            }
        }
    }
}
