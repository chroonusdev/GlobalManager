package net.chroonus.commands.server;

import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MoveCommand extends Command
{
    private static final String PERMISSION = Permissions.MOVE.toString();

    public MoveCommand()
    {
        super("move", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 2)
                {
                    String targetName = args[0];
                    String serverName = args[1];

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(targetName);
                    ServerInfo serverInfo = ProxyServer.getInstance().getServers().get(serverName);

                    Prefixes errorPrefix = Prefixes.ERROR;

                    if (target == null)
                    {
                        player.sendMessage(new TextComponent(errorPrefix + "Der Spieler §e" + targetName + " §c ist aktuell nicht online."));

                    } else if (serverInfo == null)
                    {
                        player.sendMessage(new TextComponent(errorPrefix + "Der Server §e" + serverName + " §c existiert nicht."));

                    } else if (player.getName().equals(target.getName()))
                    {
                        player.sendMessage(new TextComponent(errorPrefix + "§7Du kannst dich §cnicht §7selbst moven."));

                    } else
                    {
                        target.connect(serverInfo);

                        player.sendMessage(
                                new TextComponent(Prefixes.SERVER + target.getDisplayName() + " §7wurde auf den Server §e" + serverInfo.getName() + " §7gesendet.")
                        );

                        target.sendMessage(
                                new TextComponent(Prefixes.SERVER + "Du wurdest von " + player.getDisplayName() + " §7auf den Server §e" + serverInfo.getName() + " §7gesendet.")
                        );
                    }


                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/move §7[§cName§7] [§cServer§7]"));
                }
            }
        }
    }
}
