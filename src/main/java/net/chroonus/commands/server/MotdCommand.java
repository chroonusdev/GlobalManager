package net.chroonus.commands.server;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.MotdConfig;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MotdCommand extends Command
{
    private static final String PERMISSION = Permissions.MOTD.toString();

    public MotdCommand()
    {
        super("motd", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            MotdConfig motdConfig = Main.getInstance().getMotdConfig();

            if (player.hasPermission(PERMISSION))
            {

                if (args.length == 0)
                {
                    player.sendMessage(new TextComponent(Prefixes.SERVER + "Die aktuelle §eMOTD§7: "
                            + "\n \n" +
                            motdConfig.getFirstLine()
                            + "\n" +
                            motdConfig.getSecondLine()));

                } else if (args.length > 1)
                {
                    String lineNr = args[0];
                    StringBuilder message = new StringBuilder();


                    for (int i = 1; i < args.length; i++)
                    {
                        message.append(args[i].replaceAll("&", "§"));
                        message.append(" ");
                    }

                    if (lineNr.equals("1"))
                    {
                        motdConfig.setFirstLine(message.toString());

                        player.sendMessage(new TextComponent(Prefixes.SERVER + "Du hast die erste Zeile der §eMOTD §7gesetzt auf: \n\n" + message.toString()));
                    } else if (lineNr.equals("2"))
                    {
                        motdConfig.setSecondLine(message.toString());

                        player.sendMessage(new TextComponent(Prefixes.SERVER + "Du hast die zweite Zeile der §eMOTD §7gesetzt auf: \n\n" + message.toString()));
                    } else
                    {
                        player.sendMessage(new TextComponent(Prefixes.USAGE + "/motd <1/2> <Message>"));
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/motd <1/2> <Message>"));
                }
            }
        }
    }
}
