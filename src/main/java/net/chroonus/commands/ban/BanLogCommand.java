package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.ban.BanHandler;
import net.chroonus.chat.Prefixes;
import net.chroonus.database.datatypes.TableRow;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.List;

public class BanLogCommand extends Command
{
    private static final String PERMISSION = Permissions.BANLOG.toString();

    public BanLogCommand()
    {
        super("banlog", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];
                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        player.sendMessage(new TextComponent(""));
                        player.sendMessage(new TextComponent(Prefixes.BANLOG + "§eInformationen zu §8" + name + "§e:"));

                        if (BanHandler.getBanLog(uuid) == null)
                        {
                            player.sendMessage(new TextComponent(Prefixes.BANLOG + "Der Spieler hat keine Einträge!"));
                        } else
                        {
                            List<TableRow> tableRows = BanHandler.getBanLog(uuid);

                            for (TableRow row : tableRows)
                            {
                                String type = row.getMembers().get(0);
                                String banner = row.getMembers().get(2);
                                String start = row.getMembers().get(4);
                                String duration = row.getMembers().get(5);
                                String reason = row.getMembers().get(6);

                                player.sendMessage(new TextComponent("§7- §c" + type + "§7: §6" + reason + "§7| Von: §8" + banner + "\n §7 Daten: §a" + start + ((duration.equals("") || duration == null) ? "" : " §7| Dauer: §c" + duration) + "\n"));
                            }

                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/banlog §7[§cName§7]"));
                }
            }
        }
    }
}
