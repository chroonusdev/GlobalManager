package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.ban.BanHandler;
import net.chroonus.chat.ChatMessages;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WarnCommand extends Command
{
    private static final String PERMISSION = Permissions.WARN.toString();

    public WarnCommand()
    {
        super("warn", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 1)
                {
                    String name = args[0];
                    StringBuilder reason = new StringBuilder();

                    for (int i = 1; i < args.length; i++)
                    {
                        reason.append(args[i]).append(" ");
                    }

                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

                        if (target != null)
                        {
                            int warnCounter = BanHandler.getWarnCounter(uuid);
                            if (warnCounter >= 2)
                            {
                                player.sendMessage(new TextComponent(Prefixes.WARN + "Der Spieler §7" + name + " §7hat die maximale Anzahl von §e3 §7Warns erreicht und wird somit §4permanent §7dafür gebannt!"));
                                BanHandler.removeWarns(uuid);
                                BanHandler.ban(target, player, reason.toString(), -1);
                            } else
                            {
                                BanHandler.warn(target, player, reason.toString());
                                ChatMessages.sendWarnInformation(target, player, PERMISSION, reason.toString(), warnCounter+1);
                            }
                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cist aktuell nicht online."));
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/warn §7[§cName§7] [§cGrund§7]"));
                }
            }
        }
    }
}
