package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.ban.BanHandler;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ClearBanlogCommand extends Command
{
    private static final String PERMISSION = Permissions.CLEAR_BANLOG.toString();

    public ClearBanlogCommand()
    {
        super("clearBanlog", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];
                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));

                    } else
                    {
                        if (BanHandler.getBanLog(uuid) != null)
                        {
                            BanHandler.clearBanlog(uuid);
                            player.sendMessage(new TextComponent(Prefixes.BANLOG_PLAYER + "Der Banlog von §8" + name + " §7wurde gelöscht."));
                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §chat bereits einen leeren Banlog."));
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/clearbanlog §7[§cName§7]"));
                }
            }
        }
    }
}
