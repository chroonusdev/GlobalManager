package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.ban.BanHandler;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class RemoveWarnCommand extends Command
{
    private static final String PERMISSION = Permissions.REMOVE_WARN.toString();

    public RemoveWarnCommand()
    {
        super("removeWarn", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];
                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        int warnCounter = BanHandler.getWarnCounter(uuid);

                        if (warnCounter == 0)
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §chat bereits §e0 §cVerwarnungen."));
                        } else
                        {
                            BanHandler.removeWarn(uuid);
                            player.sendMessage(new TextComponent(Prefixes.WARN + "Du hast §8" + name + "§7 einen Warn entzogen."));
                            player.sendMessage(new TextComponent(Prefixes.WARN + "Die Anzahl der Verwarnungen beträgt nun: §e" + (warnCounter - 1)));
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/removeWarn §7[§cName§7]"));
                }
            }
        }
    }
}
