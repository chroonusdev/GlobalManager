package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.ban.BanHandler.kick;
import static net.chroonus.chat.ChatMessages.sendKickInformation;

public class KickCommand extends Command
{
    private static final String PERMISSION = Permissions.KICK.toString();

    public KickCommand()
    {
        super("kick", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 1)
                {
                    String name = args[0];
                    StringBuilder reason = new StringBuilder();

                    for (int i = 1; i < args.length; i++)
                    {
                        reason.append(args[i]).append(" ");
                    }


                    if (Main.getInstance().getUUID(name) == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

                        if (target != null)
                        {
                            kick(target, player, reason.toString());
                            sendKickInformation(target, player, PERMISSION, reason.toString());
                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §8" + name + " §cist nicht online."));
                        }

                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/kick §7[§cName§7] §7[§cGrund§7]"));
                }
            }
        }
    }
}
