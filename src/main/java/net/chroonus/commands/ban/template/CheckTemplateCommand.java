package net.chroonus.commands.ban.template;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.TemplateConfig;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CheckTemplateCommand extends Command
{
    private static final String PERMISSION = Permissions.CHECK_TEMPLATE.toString();

    public CheckTemplateCommand()
    {
        super("checkTemplate", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];

                    TemplateConfig config = Main.getInstance().getTemplateConfig();
                    TemplateConfig.Template template = config.getTemplate(name);

                    if (template != null)
                    {
                        String string = Prefixes.TEMPLATE + "§eInformationen zum Template §b" + name + "\n" +
                                "§7» Erstellt am: §a" + template.getCreatedAt() + "\n" +
                                "§7» Erstellt von: §8" + template.getCreator() + "\n" +
                                "§7» Permission: §a" + template.getPermission() + "\n" +
                                "§7» Bann-Dauer: §c" + template.getDuration() + "\n" +
                                "§7» Bann-Grund: §6" + template.getReason() + "\n";

                        player.sendMessage(new TextComponent(""));
                        player.sendMessage(new TextComponent(string));
                    } else
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Das Template §b" + name.toUpperCase() + " §cexistiert nicht."));
                    }

                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/checkTemplate §7[§cTemplate§7]"));
                }
            }
        }
    }
}
