package net.chroonus.commands.ban.template;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.TemplateConfig;
import net.chroonus.permission.Permissions;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.config.TemplateConfig.Template;

public class CreateTemplateCommand extends Command
{
    private static final String PERMISSION = Permissions.CREATE_TEMPLATE.toString();

    public CreateTemplateCommand()
    {
        super("createTemplate", PERMISSION, "ct");
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 2)
                {
                    String name = args[0];
                    String permission = "GlobalSystem." + name;
                    String duration = args[1];
                    StringBuilder reason = new StringBuilder();

                    for (int i = 2; i < args.length; i++)
                    {
                        reason.append(args[i]);
                        reason.append(" ");
                    }

                    if (!TimeHelper.isValueable(duration))
                    {
                        player.sendMessage(new TextComponent(Prefixes.USAGE + "/createTemplate §7[§cTemplate§7] §7[§cDauer§7] §7[§cGanzer Grund§7]"));
                        player.sendMessage(new TextComponent(Prefixes.TIME_UNITS + "§bs§7, §bm§7, §bh§7, §bd§7, §bw§7"));
                    }

                    TemplateConfig config = Main.getInstance().getTemplateConfig();

                    Template template = new Template();
                    template.setName(name);
                    template.setPermission(permission);
                    template.setDuration(duration);
                    template.setReason(reason.toString());
                    template.setCreatedAt(Main.getInstance().getFormatedDate());
                    template.setCreator(player.getName());

                    if (!config.containsTemplate(name))
                    {
                        config.getTemplates().add(template);

                        String message = Prefixes.TEMPLATE + "Das Template §b" + name + " §7wurde erstellt." + "\n" +
                                "§7» Bann-Dauer: §c" + duration + "\n" +
                                "§7» Bann-Grund: §6" + reason + " \n" +
                                "§7» Permission: §a" + permission;

                        player.sendMessage(new TextComponent(""));
                        player.sendMessage(new TextComponent(message));
                    } else
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Das Template §b" + name + " §cexistiert bereits."));
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/createTemplate §7[§cTemplate§7] §7[§cDauer§7] §7[§cGanzer Grund§7]"));
                    player.sendMessage(new TextComponent(Prefixes.TIME_UNITS + "§bs§7, §bm§7, §bh§7, §bd§7, §bw§7"));
                }
            }
        }
    }
}
