package net.chroonus.commands.ban.template;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.TemplateConfig;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ListTemplateCommand extends Command
{
    public static final String PERMISSION = Permissions.LIST_TEMPLATE.toString();

    public ListTemplateCommand()
    {
        super("listtemplate", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (sender.hasPermission(PERMISSION))
            {
                TemplateConfig config = Main.getInstance().getTemplateConfig();

                player.sendMessage(new TextComponent(""));
                player.sendMessage(new TextComponent(Prefixes.SERVER + "Liste der §eTemplates§7:"));

                if (config.getTemplates().isEmpty())
                {
                    player.sendMessage(new TextComponent(Prefixes.SERVER + "Es sind keine Templates vorhanden."));
                } else
                {
                    for (TemplateConfig.Template template : config.getTemplates())
                    {
                        player.sendMessage(new TextComponent(Prefixes.SERVER + "§8- §e" + template.getName()));
                    }
                }
            }
        }
    }
}
