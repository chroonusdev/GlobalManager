package net.chroonus.commands.ban.template;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.TemplateConfig;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class DeleteTemplateCommand extends Command
{
    private static final String PERMISSION = Permissions.DELETE_TEMPLATE.toString();

    public DeleteTemplateCommand()
    {
        super("deleteTemplate", PERMISSION);
    }

    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];

                    TemplateConfig config = Main.getInstance().getTemplateConfig();

                    if (config.deleteTemplate(name))
                    {
                        player.sendMessage(new TextComponent(Prefixes.TEMPLATE + "Das Template §b" + name.toUpperCase() + " §7wurde gelöscht."));
                    } else
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Das Template §b" + name.toUpperCase() + " §cexistiert nicht."));
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/deleteTemplate §7[§cName§7]"));
                }
            }
        }
    }
}
