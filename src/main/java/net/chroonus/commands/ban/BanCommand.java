package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.chat.ChatMessages;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.ban.BanHandler.ban;
import static net.chroonus.ban.BanHandler.isBanned;

public class BanCommand extends Command
{
    private static final String PERMISSION = Permissions.BAN.toString();

    public BanCommand()
    {
        super("ban", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 1)
                {
                    String name = args[0];
                    StringBuilder reason = new StringBuilder();

                    for (int i = 1; i < args.length; i++)
                    {
                        reason.append(args[i]).append(" ");
                    }

                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

                        if (!isBanned(uuid))
                        {
                            if (target == null)
                            {
                                ban(name, uuid, player, reason.toString(), -1);
                                ChatMessages.sendBanInformation(name, player, PERMISSION, "PERMANENT", reason.toString());
                            } else
                            {
                                ban(target, player, reason.toString(), -1);
                                ChatMessages.sendBanInformation(target, player, PERMISSION, "PERMANENT", reason.toString());
                            }


                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §8" + name + " §cist bereits gebannt."));
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/ban §7[§cName§7] §7[§cGrund§7]"));
                }

            }
        }
    }
}

