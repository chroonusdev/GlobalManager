package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.chat.ChatMessages;
import net.chroonus.chat.Prefixes;
import net.chroonus.config.TemplateConfig;
import net.chroonus.permission.Permissions;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.ban.BanHandler.ban;
import static net.chroonus.ban.BanHandler.isBanned;

public class BanTemplateCommand extends Command
{
    private final static String PERMISSION = Permissions.BAN_TEMPLATE.toString();

    public BanTemplateCommand()
    {
        super("banTemplate", PERMISSION, "bt");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (sender.hasPermission(PERMISSION))
            {
                TemplateConfig config = Main.getInstance().getTemplateConfig();

                if (args.length > 2)
                {
                    String name = args[0];
                    String template = args[1];
                    String uuid = Main.getInstance().getUUID(name);

                    StringBuilder extraMessage = new StringBuilder();
                    for (int i = 2; i < args.length; i++)
                    {
                        extraMessage.append(args[i].replaceAll("&", "§")).append(" ");
                    }

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        if (config.containsTemplate(template))
                        {
                            TemplateConfig.Template usingTemplate = config.getTemplate(template);

                            if (player.hasPermission(usingTemplate.getPermission())
                                    || player.hasPermission(Permissions.BAN_TEMPLATE_ALL.toString()))
                            {
                                ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

                                String reason = usingTemplate.getReason() + extraMessage.toString();
                                String duration = usingTemplate.getDuration();

                                if (!isBanned(uuid))
                                {
                                    if (target == null)
                                    {
                                        ban(name, uuid, player, reason, TimeHelper.calculateSeconds(duration));
                                        ChatMessages.sendBanInformation(name, player, PERMISSION, duration, reason);
                                    } else
                                    {
                                        ban(target, player, reason, TimeHelper.calculateSeconds(duration));
                                        ChatMessages.sendBanInformation(target, player, PERMISSION, duration, reason);
                                    }


                                } else
                                {
                                    player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §8" + name + " §cist bereits gebannt."));
                                }
                            } else
                            {
                                player.sendMessage(new TextComponent(Prefixes.ERROR + "Du darfst das Template §7" + template + " §cnicht benutzen!"));
                            }
                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Das Template §7" + template + " §cexistiert nicht!"));
                            printAllTemplates(player);
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/bantemplate §7[§cName§7] [§cTemplate§7]"));
                    printAllTemplates(player);
                }
            }
        }
    }

    private void printAllTemplates(ProxiedPlayer player)
    {
        TemplateConfig config = Main.getInstance().getTemplateConfig();

        StringBuilder templates = new StringBuilder();

        int i = 0;
        for (TemplateConfig.Template element : config.getTemplates())
        {
            templates.append("§b" + element.getName());
            if (i < config.getTemplates().size() - 1)
            {
                templates.append("§7, ");
            }
            i++;
        }

        player.sendMessage(new TextComponent(Prefixes.TEMPLATE_SAMPLES + templates.toString()));
    }
}
