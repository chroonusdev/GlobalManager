package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.chat.ChatMessages;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.ban.BanHandler.ban;
import static net.chroonus.ban.BanHandler.isBanned;

public class TempBanCommand extends Command
{
    private static final String PERMISSION = Permissions.TEMP_BAN.toString();

    public TempBanCommand()
    {
        super("tempban", PERMISSION, "tban");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length > 2)
                {
                    String name = args[0];
                    String duration = args[1];
                    StringBuilder reason = new StringBuilder();

                    for (int i = 2; i < args.length; i++)
                    {
                        reason.append(args[i]).append(" ");
                    }

                    String uuid = Main.getInstance().getUUID(name);

                    if (!TimeHelper.isValueable(duration))
                    {
                        player.sendMessage(new TextComponent(Prefixes.USAGE + "/tempban §7[§cName§7] [§cDauer§7] [§cGrund§7]"));
                        player.sendMessage(new TextComponent(Prefixes.TIME_UNITS + "§bs§7, §bm§7, §bh§7, §bd§7, §bw§7"));
                    } else
                    {
                        if (uuid == null)
                        {
                            player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                        } else
                        {
                            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

                            if (!isBanned(uuid))
                            {
                                if (target == null)
                                {
                                    ban(name, uuid, player, reason.toString(), TimeHelper.calculateSeconds(duration));
                                    ChatMessages.sendBanInformation(name, player, PERMISSION, duration, reason.toString());
                                } else
                                {
                                    ban(target, player, reason.toString(), TimeHelper.calculateSeconds(duration));
                                    ChatMessages.sendBanInformation(target, player, PERMISSION, duration, reason.toString());
                                }


                            } else
                            {
                                player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §8" + name + " §cist bereits gebannt."));
                            }
                        }
                    }

                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/tempban §7[§cName§7] [§cDauer§7] [§cGrund§7]"));
                    player.sendMessage(new TextComponent(Prefixes.TIME_UNITS + "§bs§7, §bm§7, §bh§7, §bd§7, §bw§7"));
                }
            }
        }
    }
}
