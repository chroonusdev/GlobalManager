package net.chroonus.commands.ban;

import net.chroonus.Main;
import net.chroonus.chat.Prefixes;
import net.chroonus.permission.Permissions;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.chroonus.ban.BanHandler.*;

public class CheckCommand extends Command
{
    private static final String PERMISSION = Permissions.CHECK.toString();

    public CheckCommand()
    {
        super("check", PERMISSION);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if (sender instanceof ProxiedPlayer)
        {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission(PERMISSION))
            {
                if (args.length == 1)
                {
                    String name = args[0];
                    String uuid = Main.getInstance().getUUID(name);

                    if (uuid == null)
                    {
                        player.sendMessage(new TextComponent(Prefixes.ERROR + "Der Spieler §7" + name + " §cexistiert nicht."));
                    } else
                    {
                        player.sendMessage(new TextComponent(""));
                        player.sendMessage(new TextComponent(Prefixes.CHECK + "§eInformationen zu §7" + name + "§e:"));
                        player.sendMessage(new TextComponent(Prefixes.WARN + "Verwarnungen: §e" + getWarnCounter(uuid)));

                        if (isBanned(uuid))
                        {
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Gebannt von §8" + getBanner(uuid)));
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Grund: §e" + getReason(uuid)));
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Gebannt am: §a" + getStartDate(uuid)));
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Dauer: §a" + TimeHelper.timeToSimpleString(getDuration(uuid))));
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Verbleibend: §a" + getRemainingTime(uuid)));
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Gebannt bis: §a" + getEndDate(uuid)));
                        } else
                        {
                            player.sendMessage(new TextComponent(Prefixes.BAN + "Dieser Spieler ist aktuell nicht gebannt."));
                        }
                    }
                } else
                {
                    player.sendMessage(new TextComponent(Prefixes.USAGE + "/check §7[§cName§7]"));
                }
            }
        }
    }
}
