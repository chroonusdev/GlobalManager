package net.chroonus.config;

import lombok.Getter;
import lombok.Setter;

public class MotdConfig implements Config
{
    @Getter
    @Setter
    private String firstLine = "";

    @Getter
    @Setter
    private String secondLine = "";
}
