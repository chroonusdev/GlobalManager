package net.chroonus.config;

import lombok.Getter;
import lombok.Setter;

public class DatabaseConfig implements Config
{

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String database;

    @Getter
    @Setter
    private String host;

    @Getter
    @Setter
    private int port;

}
