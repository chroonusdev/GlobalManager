package net.chroonus.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TemplateConfig implements Config
{

    @Getter
    @Setter
    private List<Template> templates = new ArrayList<>();

    public boolean deleteTemplate(String name)
    {
        for (Template element : templates)
        {
            if (element.getName().equalsIgnoreCase(name))

            {
                templates.remove(element);

                return true;
            }
        }

        return false;
    }

    public Template getTemplate(String name)
    {
        for (Template element : templates)
        {
            if (element.getName().equalsIgnoreCase(name))
            {
                return element;
            }
        }

        return null;
    }

    public boolean containsTemplate(String name)
    {
        for (Template element : templates)
        {
            if (element.getName() != null)
            {
                if (element.getName().equalsIgnoreCase(name))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public List<Template> getTemplates()
    {
        return templates;
    }

    @AllArgsConstructor
    public static class Template
    {
        @Getter
        @Setter
        private String creator;

        @Getter
        @Setter
        private String createdAt;

        @Getter
        @Setter
        private String name;

        @Getter
        @Setter
        private String reason;

        @Getter
        @Setter
        private String duration;

        @Getter
        @Setter
        private String permission;
    }
}
