package net.chroonus.config;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class RankConfig implements Config
{
    @Getter
    @Setter
    private List<Rank> ranks = new ArrayList<>();


    @AllArgsConstructor
    public static class Rank
    {

        @Getter
        @Setter
        private String permission;

        @Getter
        @Setter
        private String color;

        @Getter
        @Setter
        private int priority;
    }
}
