package net.chroonus.config;

import net.chroonus.Main;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ConfigLoader
{

    public static <T extends Config> T load(File file, Class<T> clazz) throws IOException
    {
        if (!file.exists())
        {
            file.createNewFile();
        }


        return Main.getInstance().getGson().fromJson(new FileReader(file), clazz);
    }

    public static <T extends Config> void save(File file, T element) throws IOException
    {
        String json = Main.getInstance().getGson().toJson(element);

        FileWriter writer = new FileWriter(file);
        writer.write(json);
        writer.close();
    }
}
