package net.chroonus.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class UserConfig implements Config
{

    @Getter
    private List<User> users = new ArrayList<>();

    public boolean containsUser( String uuid )
    {
        for ( User element : users )
        {
            if ( element.getUuid().equalsIgnoreCase( uuid ) )
            {
                return true;
            }
        }

        return false;
    }

    public boolean addUser( String name, String uuid, String rank, int priority )
    {
        if ( !containsUser( uuid ) )
        {
            User user = new User( name, uuid, rank, priority );
            users.add( user );

            return true;
        }

        return false;
    }

    public User getUser( String uuid )
    {
        if ( containsUser( uuid ) )
        {
            for ( User element : users )
            {
                if ( element.getUuid().equalsIgnoreCase( uuid ) )
                {
                    return element;
                }
            }
        }

        return null;
    }


    @AllArgsConstructor
    public static class User
    {
        @Getter
        @Setter
        private String name;

        @Getter
        @Setter
        private String uuid;

        @Getter
        @Setter
        private String rank;

        @Getter
        @Setter
        private int priority;
    }
}
