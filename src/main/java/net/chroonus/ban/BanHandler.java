package net.chroonus.ban;

import net.chroonus.Main;
import net.chroonus.database.DatabaseDriver;
import net.chroonus.database.datatypes.TableRow;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Date;
import java.util.List;

import static net.chroonus.chat.ChatMessages.*;

public class BanHandler
{

    private static DatabaseDriver databaseDriver;

    static
    {
        databaseDriver = Main.getInstance().getDatabase();
    }

    /**
     * Method for banning a certain Player if he is connected from the network
     *
     * @param player  the player to be banned
     * @param banner  the player whom executed the ban
     * @param reason  the reason for why the player is banned
     * @param seconds the duration
     */
    public static void ban( ProxiedPlayer player, ProxiedPlayer banner, String reason, long seconds )
    {

        ban( player.getName(), player.getUniqueId().toString(), banner, reason, seconds );

        if ( player.isConnected() )
        {
            player.disconnect( new TextComponent( ( seconds == -1 ) ? banMessage( reason ) : tempBanMessage( TimeHelper.timeToString( seconds * 1000 + 1 ), reason, getRemainingTime( player.getUniqueId().toString() ) ) ) );
        }
    }

    /**
     * Method for banning a certain Player if he is not connected
     *
     * @param name    the name of the player to be banned
     * @param uuid    the uuid of the player to be banned
     * @param banner  the player whom executed the ban
     * @param reason  the reason for why the player is banned
     * @param seconds the duration
     */
    public static void ban( String name, String uuid, ProxiedPlayer banner, String reason, long seconds )
    {

        long end;
        long current = System.currentTimeMillis();
        long millis = seconds * 1000 + 1;
        if ( seconds == -1 )
        {
            end = seconds;
        } else
        {
            end = current + millis;

        }

        databaseDriver.insertItem( Main.getInstance().getBannedPlayers(), name, banner.getName(), uuid, String.valueOf( millis ), String.valueOf( current ), String.valueOf( end ), reason );
        databaseDriver.insertItem( Main.getInstance().getBanLog(), "BAN", name, banner.getName(), uuid, Main.getInstance().getFormatedDate(), TimeHelper.timeToSimpleString( millis ), reason );

    }

    /**
     * Method for kicking a certain Player from the network
     *
     * @param player the player to be kicked
     * @param kicker the player whom executed the kick
     * @param reason the reason for why the player is kicked
     */
    public static void kick( ProxiedPlayer player, ProxiedPlayer kicker, String reason )
    {
        databaseDriver.insertItem( Main.getInstance().getBanLog(), "KICK", player.getName(), kicker.getName(), player.getUniqueId().toString().replaceAll( "-", "" ), Main.getInstance().getFormatedDate(), "", reason );

        if ( player.isConnected() )
        {
            player.disconnect( new TextComponent( kickMessage( reason ) ) );
        }
    }

    /**
     * Method for unbanning a certain player from the network
     *
     * @param name   the name of the player to get unbanned
     * @param uuid   the uuid of the player to get unbanned
     * @param banner the player whom executed the unban
     * @param reason the reason for why the player is unbanned
     */
    public static void unban( String name, String uuid, ProxiedPlayer banner, String reason )
    {
        databaseDriver.deleteItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid );
        databaseDriver.insertItem( Main.getInstance().getBanLog(), "UNBAN", name, banner.getName(), uuid, Main.getInstance().getFormatedDate(), "", reason );
    }

    /**
     * Method for getting the amount of warns a player has earned
     *
     * @param uuid the uuid of the player to get the warn count
     * @return the amount of how often the player has earned a warn
     */
    public static int getWarnCounter( String uuid )
    {
        return databaseDriver.getColumnCount( Main.getInstance().getBanLog(), uuid, "TYPE", "TYPE", "WARN" );
    }

    /**
     * Method for warning a player because of discipline issues
     *
     * @param player the player to get warned
     * @param warner the player whom executed the warn
     * @param reason the reason why the player is getting warned
     */
    public static void warn( ProxiedPlayer player, ProxiedPlayer warner, String reason )
    {
        databaseDriver.insertItem( Main.getInstance().getBanLog(), "WARN", player.getName(), warner.getName(), player.getUniqueId().toString().replaceAll( "-", "" ), Main.getInstance().getFormatedDate(), "", reason );
    }

    /**
     * Method for decreasing the amount of warns a player has earned by one
     *
     * @param uuid the uuid of the player to get a reduction
     */
    public static void removeWarn( String uuid )
    {
        databaseDriver.deleteItemValueLimited( Main.getInstance().getBanLog(), "UUID", uuid, "TYPE", "WARN", 1 );
    }

    /**
     * Method for removing all warns of a player
     *
     * @param uuid the uuid of the player to get a deletion
     */
    public static void removeWarns( String uuid )
    {
        databaseDriver.deleteItemValue( Main.getInstance().getBanLog(), "UUID", uuid, "TYPE", "WARN" );
    }

    /**
     * Method for getting the whole banlog of a certain player
     * including all violations
     *
     * @param uuid the uuid of the player
     * @return a list of database TableRows for all entries
     */
    public static List<TableRow> getBanLog( String uuid )
    {
        return databaseDriver.getItemValues( Main.getInstance().getBanLog(), "UUID", uuid );
    }

    /**
     * Method for clearing the whole banlog of a certain player
     *
     * @param uuid the uuid of the player to get a clear
     */
    public static void clearBanlog( String uuid )
    {
        databaseDriver.deleteItemValue( Main.getInstance().getBanLog(), "UUID", uuid );
    }

    /**
     * Method for checking if a certain player is currently banned
     * or not
     *
     * @param uuid the uuid of the player to be checked
     * @return if player is currently banned or not
     */
    public static boolean isBanned( String uuid )
    {

        String banned = databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "END" );


        if ( banned == null )
        {
            return false;
        }


        long current = System.currentTimeMillis();

        long end = getEnd( uuid );

        if ( current > end && end != -1 )
        {
            databaseDriver.deleteItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid );

            return false;
        }

        return true;
    }

    /**
     * Method for getting the reason of the punishment
     *
     * @param uuid the uuid of the player
     * @return the reason why the player has been getting banned
     */
    public static String getReason( String uuid )
    {
        return databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "REASON" );
    }

    /**
     * Method for getting the end of the punishment
     * in milliseconds
     *
     * @param uuid the uuid of the player
     * @return the end in System-milliseconds
     */
    public static long getEnd( String uuid )
    {
        return Long.parseLong( databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "END" ) );
    }

    /**
     * Method for getting the duration of the punishment
     * in milliseconds
     *
     * @param uuid the uuid of the player
     * @return the duration in milliseconds
     */
    public static long getDuration( String uuid )
    {
        return Long.parseLong( databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "DURATION" ) );
    }

    /**
     * Method for getting the start of the punishment
     * in milliseconds
     *
     * @param uuid the uuid of the player
     * @return the start in System-milliseconds
     */
    public static long getStart( String uuid )
    {
        return Long.parseLong( databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "START" ) );
    }


    /**
     * Method for getting the name of the executor of the punishment
     *
     * @param uuid the uuid of the player to get the executor from
     * @return the name of the executor
     */
    public static String getBanner( String uuid )
    {
        return databaseDriver.getItemValue( Main.getInstance().getBannedPlayers(), "UUID", uuid, "BANNER" );
    }

    /**
     * Method for getting the start date of the punishment of
     * a certain player
     *
     * @param uuid the uuid of the player
     * @return the formatted date when the punishment happened
     */
    public static String getStartDate( String uuid )
    {
        Date date = new Date( getStart( uuid ) );

        return Main.getInstance().getDateFormat().format( date );
    }

    /**
     * Method for getting the end date of the punishment of
     * a certain player
     *
     * @param uuid the uuid of the player
     * @return the formatted date when the punishment expires
     */
    public static String getEndDate( String uuid )
    {
        if ( getEnd( uuid ) < 0 )
        {
            return "ENDE DER WELT";
        }

        Date date = new Date( getEnd( uuid ) );

        return Main.getInstance().getDateFormat().format( date );
    }

    /**
     * Method for getting the remaining time of a punishment
     * of a certain player
     *
     * @param uuid the uuid of the player
     * @return the formatted Time String of the remaining time
     */
    public static String getRemainingTime( String uuid )
    {
        long current = System.currentTimeMillis();
        long end = getEnd( uuid );

        if ( end == -1 )
        {
            return "PERMANENT";
        }

        long millis = end - current;

        return TimeHelper.timeToString( millis );
    }

}
