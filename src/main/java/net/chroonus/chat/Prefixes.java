package net.chroonus.chat;

import lombok.Getter;

public enum Prefixes
{
    TL_LOCAL("§7[§cTL-Lokal§7]"),
    BUILDER_LOCAL("§7[§3Lokal§7]"),
    TL_GLOBAL("§7[§cTL-Global§7]"),
    BUILDER_GLOBAL("§7[§3Global§7]"),
    SERVER("§7[§bBauserver§7] "),
    BANLOG("§7[§6BANLOG§7] "),
    BANLOG_PLAYER("§7[§6Banlog§7] "),
    BAN("§7[§6Ban§7] "),
    KICK("§7[§6Kick§7] "),
    UNBAN("§7[§6Unban§7] "),
    CHECK("§7[§6CHECK§7] "),
    WARN("§7[§6Warn§7] "),
    WARN_PLAYER("§7[§4WARN§7]§c "),
    ERROR("§7[§4Fehler§7]§c "),
    TEMPLATE("§7[§6Template§7] "),
    TEMPLATE_SAMPLES("§4Templates§7: "),
    USAGE("§4Benutzung§7: §c"),
    TIME_UNITS("§4Zeitangaben§7: ");

    @Getter
    private String prefix;

    Prefixes(String prefix)
    {
        this.prefix = prefix;
    }

    @Override
    public String toString()
    {
        return prefix;
    }
}
