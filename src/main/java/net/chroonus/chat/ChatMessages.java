package net.chroonus.chat;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;

public class ChatMessages
{


    public static void sendLocalTeamMessage(ProxiedPlayer sender, String teamPermission, Prefixes prefix, String message)
    {
        Server server = sender.getServer();

        for (ProxiedPlayer proxiedPlayer : server.getInfo().getPlayers())
        {
            if (proxiedPlayer.hasPermission(teamPermission))
            {
                proxiedPlayer.sendMessage(new TextComponent(prefix + " " + sender.getDisplayName() + " §7» §e" + message));
            }
        }
    }

    public static void sendGlobalTeamMessage(ProxiedPlayer sender, String teamPermission, Prefixes prefix, String message)
    {
        Server senderServer = sender.getServer();

        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(teamPermission))
            {
                proxiedPlayer.sendMessage(new TextComponent(prefix + " §7[" + senderServer.getInfo().getName() + "§7] " + sender.getDisplayName() + " §7» §e" + message));
            }
        }
    }

    public static void sendBanInformation(ProxiedPlayer target, ProxiedPlayer sender, String permission, String duration, String reason)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.BAN + target.getDisplayName() + " §7wurde von " + sender.getDisplayName() + " §7gebannt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.BAN + "Grund: §6" + reason + " §7| Dauer: §6" + duration));
            }
        }
    }

    public static void sendBanInformation(String playername, ProxiedPlayer sender, String permission, String duration, String reason)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.BAN + "§8" + playername + " §7wurde von " + sender.getDisplayName() + " §7gebannt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.BAN + "Grund: §6" + reason + " §7| Dauer: §6" + duration));
            }
        }
    }

    public static void sendKickInformation(ProxiedPlayer target, ProxiedPlayer sender, String permission, String reason)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.KICK + target.getDisplayName() + " §7wurde von " + sender.getDisplayName() + " §7gekickt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.KICK + "Grund: §6" + reason));
            }
        }
    }

    public static void sendUnbanInformation(ProxiedPlayer target, ProxiedPlayer sender, String permission, String reason)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.UNBAN + target.getDisplayName() + " §7wurde von " + sender.getDisplayName() + " §7entbannt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.UNBAN + "Grund: §6" + reason));
            }
        }
    }

    public static void sendUnbanInformation(String playername, ProxiedPlayer sender, String permission, String reason)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.UNBAN + "§8" + playername + " §7wurde von " + sender.getDisplayName() + " §7entbannt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.UNBAN + "Grund: §6" + reason));
            }
        }
    }

    public static void sendWarnInformation(ProxiedPlayer target, ProxiedPlayer sender, String permission, String reason, int counter)
    {
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers())
        {
            if (proxiedPlayer.hasPermission(permission))
            {
                proxiedPlayer.sendMessage(new TextComponent(""));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.WARN + target.getDisplayName() + " §7wurde von " + sender.getDisplayName() + " §7verwarnt!"));
                proxiedPlayer.sendMessage(new TextComponent(Prefixes.WARN + "Grund: §6" + reason + " §7| §6" + counter + " §7/ §63"));
            }
        }

        sendPlayerWarnInformation(target, reason, counter);
    }

    private static void sendPlayerWarnInformation(ProxiedPlayer player, String reason, int counter)
    {
        player.sendMessage(new TextComponent(""));
        player.sendMessage(new TextComponent(Prefixes.WARN_PLAYER + "Du wurdest verwarnt! Achte besser auf dein Verhalten!"));
        player.sendMessage(new TextComponent(Prefixes.WARN_PLAYER + "Grund: §6" + reason));
        player.sendMessage(new TextComponent(Prefixes.WARN_PLAYER + "Anzahl deiner Verwarnungen: §e" + counter + " §7/ §e3"));
    }

    public static String kickMessage(String reason)
    {
        return "§cDu wurdest vom Bauserver-Netzwerk gekickt! \n§7Grund: §6" + reason;
    }

    public static String tempBanMessage(String duration, String reason, String remaining)
    {
        return "§cDu wurdest für §e" + duration + " §cvom Bauserver-Netzwerk gebannt! \n\n§7Grund: §6" + reason + " \n§7Verbleibende Zeit: §e" + remaining;
    }

    public static String banMessage(String reason)
    {
        return "§cDu wurdest §4permanent §cvom GommeHD.net Bauserver-Netzwerk gebannt! \n§7Grund: §6" + reason + "\n\n§7Du kannst nur auf dem §5http://www.Discord.io/Bauevent Discord §7um Entbannung bitten.";
    }
}
