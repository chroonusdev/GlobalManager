package net.chroonus.util;

import lombok.Getter;

public class MojangUser
{

    @Getter
    private String id;

    @Getter
    private String name;
}
