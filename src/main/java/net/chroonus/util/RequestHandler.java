package net.chroonus.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestHandler
{

    public static String sendGetRequest(String str)
    {
        StringBuilder responseString = new StringBuilder();

        try
        {
            URL url = new URL(str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("accept", "application/json");

            BufferedReader stream = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String s;
            while ((s = stream.readLine()) != null)
            {
                responseString.append(s);
            }

            stream.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return responseString.toString();
    }
}
