package net.chroonus.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeHelper
{

    /**
     * Method for checking if input-string is a valueable
     * time string
     *
     * @param string the input-string to be proven
     * @return if matches regex or not
     */
    public static boolean isValueable(String string)
    {
        Pattern pattern = Pattern.compile("([0-9][w]|[1234][0-9][w]|5[0123][w])|([0-7][d])|([0-9][h]|[1][0-9][h]|2[01234][h])|([0-6][m]|[12345][0-9][m]|60m)|([0-6][s]|[12345][0-9][s]|60s)|(permanent)");
        Matcher matcher = pattern.matcher(string);

        return matcher.find(); //TODO: FIX MISMATCHES
    }

    /**
     * Method for calculating seconds from a valuable
     * time string
     *
     * @param string for the string to be calculated into seconds
     * @return the amount of seconds of the time string
     */
    public static long calculateSeconds(String string)
    {
        if (string.equalsIgnoreCase("permanent"))
        {
            return -1;
        }

        long seconds = 0;
        if (isValueable(string))
        {
            int index = 0;

            for (int i = 0; i < string.length(); i++)
            {
                String current = String.valueOf(string.charAt(i));

                try
                {

                    Integer.parseInt(current);

                } catch (NumberFormatException e)
                {
                    String sub = string.substring(index, i);

                    seconds += toSeconds(Long.parseLong(sub), current);

                    index = i + 1;
                }
            }
        }

        return seconds;
    }

    /**
     * Method for formatting the amount of milliseconds to a
     * reusable time String
     *
     * @param millis the amount of milliseconds
     * @return the formatted string
     */
    public static String timeToString(long millis)
    {
        long seconds = 0,
                minutes = 0,
                hours = 0,
                days = 0,
                weeks = 0;

        if (millis == -1) return "permanent";

        while (millis >= 1000)
        {
            millis -= 1000;
            seconds++;
        }
        while (seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }
        while (minutes >= 60)
        {
            minutes -= 60;
            hours++;
        }
        while (hours >= 24)
        {
            hours -= 24;
            days++;
        }
        while (days >= 7)
        {
            days -= 7;
            weeks++;
        }

        return format("Woche", weeks) + format("Tage", days) + format("Stunde", hours) + format("Minute", minutes) + format("Sekunde", seconds);
    }

    public static String timeToSimpleString(long millis)
    {
        long seconds = 0,
                minutes = 0,
                hours = 0,
                days = 0,
                weeks = 0;

        if (millis == -1) return "permanent";

        while (millis >= 1000)
        {
            millis -= 1000;
            seconds++;
        }
        while (seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }
        while (minutes >= 60)
        {
            minutes -= 60;
            hours++;
        }
        while (hours >= 24)
        {
            hours -= 24;
            days++;
        }
        while (days >= 7)
        {
            days -= 7;
            weeks++;
        }

        return format("w", weeks) + format("d", days) + format("h", hours) + format("m", minutes) + format("s", seconds);
    }

    /**
     * Method for formatting if unit is single or multi used
     *
     * @param str    the time unit
     * @param amount the amount of time
     * @return the formatted String
     */
    private static String format(String str, long amount)
    {
        if (amount == 0)
        {
            return "";
        }

        if (str.equals("w") || str.equals("d") || str.equals("h") || str.equals("m") || str.equals("s"))
            return amount + str;

        if (amount > 1)
        {
            str += "n";
        }

        return amount + " " + str + " ";
    }


    /**
     * Method for calculating a certain second amount of specific unit
     * Supported:
     * w(weeks)
     * d(days)
     * h(hours)
     * m(minutes)
     * s(seconds)
     *
     * @param value the value to be calculated into seconds
     * @param type  the unit of the value
     * @return the calculated amount of seconds
     */
    private static long toSeconds(long value, String type)
    {
        long multiplyer = 0;

        switch (type)
        {
            case "w":
                multiplyer = 604800; //60 * 60 * 24 * 7
                break;
            case "d":
                multiplyer = 86400; //60 * 60 * 24
                break;
            case "h":
                multiplyer = 3600; //60 * 60
                break;
            case "m":
                multiplyer = 60; //60 * 1
                break;
            case "s":
                multiplyer = 1; //1
                break;
        }

        return (value * multiplyer);
    }

}
