package net.chroonus.permission;

import lombok.Getter;

public enum Permissions
{
    BUILDER_LOCAL("GlobalSystem.Chat.BL"),
    BUILDER_GLOBAL("GlobalSystem.Chat.BG"),
    TL_LOCAL("GlobalSystem.Chat.TLL"),
    TL_GLOBAL("GlobalSystem.Chat.TLG"),
    MOVE("GlobalSystem.Move"),
    GOTO("GlobalSystem.GoTo"),
    MOTD("GlobalSystem.Motd"),
    HUB("GlobalSystem.Hub"),
    BAN("GlobalSystem.Ban"),
    TEMP_BAN("GlobalSystem.TempBan"),
    UNBAN("GlobalSystem.UnBan"),
    CHECK("GlobalSystem.Check"),
    BANLOG("GlobalSystem.BanLog"),
    CLEAR_BANLOG("GlobalSystem.ClearBanlog"),
    WARN("GlobalSystem.Warn"),
    REMOVE_WARN("GlobalSystem.RemoveWarn"),
    BAN_TEMPLATE("GlobalSystem.BanTemplate"),
    BAN_TEMPLATE_ALL("GlobalSystem.BanTemplate.All"),
    CREATE_TEMPLATE("GlobalSystem.CreateTemplate"),
    DELETE_TEMPLATE("GlobalSystem.DeleteTemplate"),
    CHECK_TEMPLATE("GlobalSystem.CheckTemplate"),
    LIST_TEMPLATE("GlobalSystem.ListTemplate"),
    KICK("GlobalSystem.Kick");

    @Getter
    private String permission;

    Permissions(String permission)
    {
        this.permission = permission;
    }

    @Override
    public String toString()
    {
        return permission;
    }
}
