package net.chroonus.database;

import net.chroonus.Main;
import net.chroonus.config.DatabaseConfig;
import net.chroonus.database.datatypes.Table;
import net.chroonus.database.datatypes.TableRow;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SQLite Database Driver
 */
public class DatabaseDriver
{
    private boolean connected;

    private String url;

    private Connection connection;

    private DatabaseConfig databaseConfig;

    public DatabaseDriver()
    {

        databaseConfig = Main.getInstance().getDatabaseConfig();

        this.url = "jdbc:mysql://" + databaseConfig.getHost() + ":" + databaseConfig.getPort() + "/" + databaseConfig.getDatabase();

    }

    public void createTable( Table table )
    {
        if ( isConnected() )
        {
            String query = "CREATE TABLE IF NOT EXISTS " + table.getName() + " " + table.getTableElements();

            updateQuery( query );
        }
    }

    /**
     * Returns the keyValue from a certain Key of the Databasetable
     * <p>
     * Example:
     * getItemValue("BannedPlayers", "UUID", "123455", "Banned")
     * will call this query:
     * SELECT * FROM BannedPlayers WHERE UUID='123455'
     * and will give back the value from the table column 'Banned'
     *
     * @param table    the table to search in
     * @param key      the key to be checked
     * @param keyValue the value to find the table column
     * @param value    the value to look after
     * @return the database value
     */
    public String getItemValue( Table table, String key, String keyValue, String value )
    {
        ResultSet resultSet = executeQuery( "SELECT * FROM " + table.getName() + " WHERE " + key.toUpperCase() + "=?", keyValue );

        try
        {
            while ( resultSet.next() )
            {
                return resultSet.getString( value );
            }
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }

        return null;
    }

    public void insertItem( Table table, String... values )
    {
        if ( isConnected() )
        {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append( "('" );

            int cnt = 0;
            for ( String value : values )
            {

                if ( cnt < ( values.length - 1 ) )
                {
                    stringBuilder.append( value ).append( "','" );
                } else
                {
                    stringBuilder.append( value );
                }

                cnt++;
            }

            stringBuilder.append( "')" );

            String query = "INSERT INTO " + table.getName() + " " + table.getTableKeys() + " VALUES " + stringBuilder.toString();

            updateQuery( query );
        }

    }

    public List<TableRow> getItemValues( Table table, String key, String keyValue )
    {
        List<TableRow> rows = new ArrayList<>();

        if ( isConnected() )
        {
            ResultSet resultSet = executeQuery( "SELECT *  FROM " + table.getName() + " WHERE " + key.toUpperCase() + "=?", keyValue );
            try
            {
                while ( resultSet.next() )
                {
                    String type = resultSet.getString( "TYPE" );
                    String username = resultSet.getString( "USERNAME" );
                    String banner = resultSet.getString( "BANNER" );
                    String uuid = resultSet.getString( "UUID" );
                    String start = resultSet.getString( "START" );
                    String duration = resultSet.getString( "DURATION" );
                    String reason = resultSet.getString( "REASON" );


                    TableRow tableRow = new TableRow( type, username, banner, uuid, start, duration, reason );
                    rows.add( tableRow );
                }
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }

        return ( rows.isEmpty() ) ? null : rows;
    }


    public void deleteItemValue( Table table, String key, String keyValue )
    {
        if ( isConnected() )
        {
            updateQuery( "DELETE FROM " + table.getName() + " WHERE " + key.toUpperCase() + "=?", keyValue );
        }
    }

    public void deleteItemValue( Table table, String key1, String keyValue1, String key2, String keyValue2 )
    {
        if ( isConnected() )
        {
            updateQuery( "DELETE FROM " + table.getName() + " WHERE " + key1.toUpperCase() + "=? AND " + key2.toUpperCase() + "=?", keyValue1, keyValue2 );
        }
    }

    public void deleteItemValueLimited( Table table, String key, String keyValue, int limit )
    {
        if ( isConnected() )
        {
            updateQuery( "DELETE FROM " + table.getName() + " WHERE " + key.toUpperCase() + "=? LIMIT " + limit, keyValue );
        }
    }

    public void deleteItemValueLimited( Table table, String key1, String keyValue1, String key2, String keyValue2, int limit )
    {
        if ( isConnected() )
        {
            updateQuery( "DELETE FROM " + table.getName() + " WHERE " + key1.toUpperCase() + "='" + keyValue1 + "' AND " + key2.toUpperCase() + "='" + keyValue2 + "' LIMIT " + limit );
        }
    }

    public int getColumnCount( Table table, String uuid, String type, String key, String keyValue )
    {
        if ( isConnected() )
        {
            ResultSet resultSet = executeQuery( "SELECT COUNT(" + type + ") FROM " + table.getName() + " WHERE UUID=? AND " + key + "=?", uuid, keyValue );

            try
            {
                while ( resultSet.next() )
                {
                    return resultSet.getInt( "COUNT(TYPE)" );
                }
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }

        return 0;
    }

    public void updateQuery( String query, String... params )
    {
        if ( isConnected() )
        {
            try
            {
                PreparedStatement preparedStatement = connection.prepareStatement( query );
                for ( int i = 0; i < params.length; i++ )
                {
                    preparedStatement.setString( i + 1, params[i] );
                }

                preparedStatement.executeUpdate();

            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }
    }

    public ResultSet executeQuery( String query, String... params )
    {
        if ( isConnected() )
        {
            try
            {
                PreparedStatement preparedStatement = connection.prepareStatement( query );

                for ( int i = 0; i < params.length; i++ )
                {
                    preparedStatement.setString( i + 1, params[i] );
                }

                return preparedStatement.executeQuery();
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }

        return null;
    }

    public boolean isConnected()
    {
        if ( connection == null )
        {
            return false;
        }

        try
        {
            if ( connection.isClosed() != connected )
            {
                connection = DriverManager.getConnection( this.url, databaseConfig.getUsername(), databaseConfig.getPassword() );

                return true;
            }
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }

        return true;
    }

    public void connect()
    {
        if ( !isConnected() )
        {
            try
            {
                connection = DriverManager.getConnection( this.url, databaseConfig.getUsername(), databaseConfig.getPassword() );

                connected = true;
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }

        }
    }


    public void disconnect()
    {
        if ( isConnected() )
        {
            try
            {
                connection.close();
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }
    }
}
