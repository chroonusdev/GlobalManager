package net.chroonus.database.datatypes;

import lombok.Getter;

public class VarChar
{
    @Getter
    private String name;

    @Getter
    private int size;

    public VarChar(String name, int size)
    {
        this.name = name;
        this.size = size;
    }

    public String toString(){
        return name + " VARCHAR(" + size + ")";
    }
}
