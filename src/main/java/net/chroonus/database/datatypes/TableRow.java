package net.chroonus.database.datatypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TableRow
{
    private List<String> members;

    public TableRow(String... values)
    {
        members = new ArrayList<>();
        members.addAll(Arrays.asList(values));
    }

    public List<String> getMembers()
    {
        return members;
    }
}
