package net.chroonus.database.datatypes;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Table
{

    @Getter
    private String name;

    @Getter
    private List<VarChar> keys;


    public Table(String name, VarChar... varChars)
    {
        this.name = name;

        keys = new ArrayList<>();

        keys.addAll(Arrays.asList(varChars));
    }

    public String getTableKeys()
    {
        StringBuilder builder = new StringBuilder();

        builder.append("(");

        for (int i = 0; i < keys.size(); i++)
        {
            VarChar key = keys.get(i);
            builder.append(key.getName());

            if (i < keys.size() - 1)
            {
                builder.append(",");
            }
        }

        builder.append(")");

        return builder.toString();
    }

    public String getTableElements()
    {
        StringBuilder builder = new StringBuilder();

        builder.append("(");

        for (int i = 0; i < keys.size(); i++)
        {
            VarChar key = keys.get(i);
            builder.append(key.toString());

            if (i < keys.size() - 1)
            {
                builder.append(",");
            }
        }

        builder.append(")");

        return builder.toString();
    }
}
