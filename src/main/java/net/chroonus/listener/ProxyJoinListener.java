package net.chroonus.listener;

import net.chroonus.Main;
import net.chroonus.config.RankConfig;
import net.chroonus.config.UserConfig;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ProxyJoinListener implements Listener
{

    @EventHandler
    public void onProxyJoin( PostLoginEvent event )
    {
        ProxiedPlayer player = event.getPlayer();
        UserConfig userConfig = Main.getInstance().getUserConfig();

        String prefix = "";
        String permission = "";
        int highest = 0;

        for ( RankConfig.Rank rank : Main.getInstance().getRankConfig().getRanks() )
        {

            if ( ( rank.getPriority() > highest ) && player.hasPermission( rank.getPermission() ) )
            {
                permission = rank.getPermission();
                highest = rank.getPriority();
                prefix = rank.getColor().replaceAll( "&", "§" );
            }
        }

        if ( userConfig.containsUser( player.getUniqueId().toString() ) )
        {
            UserConfig.User user = userConfig.getUser( player.getUniqueId().toString() );

            if ( user.getPriority() < highest )
            {
                user.setRank( permission );
                user.setPriority( highest );
            } else
            {
                //TODO: SET USER RANK PREFIX@
            }
        } else
        {
            userConfig.addUser( player.getName(), player.getUniqueId().toString(), permission, highest );
        }

        player.setDisplayName( prefix + player.getName() );

    }
}
