package net.chroonus.listener;

import net.chroonus.Main;
import net.chroonus.util.TimeHelper;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import static net.chroonus.ban.BanHandler.*;
import static net.chroonus.chat.ChatMessages.banMessage;
import static net.chroonus.chat.ChatMessages.tempBanMessage;

public class ProxyConnectListener implements Listener
{

    @EventHandler
    public void onProxyConnect(PreLoginEvent event)
    {
        final String name = event.getConnection().getName();
        final String uuid = Main.getInstance().getUUID(name);
        event.registerIntent(Main.getInstance());
        
        ProxyServer.getInstance().getScheduler().runAsync(Main.getInstance(), () ->
        {
            if (isBanned(uuid))
            {

                String reason = getReason(uuid);
                String remaining = getRemainingTime(uuid);

                String duration = TimeHelper.timeToString(getDuration(uuid));


                String kickMessage;

                if (remaining.equalsIgnoreCase("PERMANENT"))
                {
                    kickMessage = banMessage(reason);

                } else
                {
                    kickMessage = tempBanMessage(duration, reason, remaining);
                }

                event.setCancelReason(new TextComponent(kickMessage));
                event.setCancelled(true);
            }
            event.completeIntent(Main.getInstance());
        });


    }
}
