package net.chroonus.listener;

import net.chroonus.Main;
import net.chroonus.config.MotdConfig;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ProxyPingListener implements Listener
{

    @EventHandler
    public void onProxyPing(ProxyPingEvent event)
    {
        ServerPing serverPing = event.getResponse();
        MotdConfig config = Main.getInstance().getMotdConfig();

        serverPing.setDescriptionComponent(new TextComponent(config.getFirstLine().replaceAll("&", "§") + "\n§r" + config.getSecondLine().replaceAll("&", "§")));
        event.setResponse(serverPing);
    }
}
