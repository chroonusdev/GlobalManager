package net.chroonus;

import com.google.gson.Gson;
import lombok.Getter;
import net.chroonus.commands.ban.*;
import net.chroonus.commands.ban.template.CheckTemplateCommand;
import net.chroonus.commands.ban.template.CreateTemplateCommand;
import net.chroonus.commands.ban.template.DeleteTemplateCommand;
import net.chroonus.commands.ban.template.ListTemplateCommand;
import net.chroonus.commands.server.GoToCommand;
import net.chroonus.commands.server.HubCommand;
import net.chroonus.commands.server.MotdCommand;
import net.chroonus.commands.server.MoveCommand;
import net.chroonus.commands.teamchats.BuilderGlobalChatCommand;
import net.chroonus.commands.teamchats.BuilderLocalChatCommand;
import net.chroonus.commands.teamchats.TeamLeaderGlobalChatCommand;
import net.chroonus.commands.teamchats.TeamLeaderLocalChatCommand;
import net.chroonus.config.*;
import net.chroonus.database.DatabaseDriver;
import net.chroonus.database.datatypes.Table;
import net.chroonus.database.datatypes.VarChar;
import net.chroonus.listener.ProxyConnectListener;
import net.chroonus.listener.ProxyJoinListener;
import net.chroonus.listener.ProxyPingListener;
import net.chroonus.util.RequestHandler;
import net.chroonus.util.MojangUser;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * GlobalManager Plugin for Bungeecord
 * includes
 * -Ban System
 * -TeamChats
 * -Server Switching
 * -MOTD
 * <p>
 * https://docs.google.com/document/d/1dQh5hnqHAzrQe5gR0FIkmyUs771HwFZnOO_pOGgV5gs/edit
 */
public class Main extends Plugin
{

    @Getter
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @Getter
    private static Main instance;

    @Getter
    private PluginManager pluginManager;

    @Getter
    private Gson gson;

    @Getter
    private MotdConfig motdConfig;
    private File motdConfigFile;

    @Getter
    private Table bannedPlayers;

    @Getter
    private Table banLog;

    @Getter
    private TemplateConfig templateConfig;
    private File templateConfigFile;

    @Getter
    private DatabaseConfig databaseConfig;
    private File databaseConfigFile;

    @Getter
    private RankConfig rankConfig;
    private File rankConfigFile;

    @Getter
    private UserConfig userConfig;
    private File userConfigFile;

    @Getter
    private DatabaseDriver database;

    public String getFormatedDate()
    {
        Date date = new Date();

        return dateFormat.format(date);
    }

    public String getUUID(String name)
    {
        String response = RequestHandler.sendGetRequest("https://api.mojang.com/users/profiles/minecraft/" + name);

        MojangUser mojangUser = gson.fromJson(response, MojangUser.class);

        if ( mojangUser == null)
        {
            return null;
        }

        return mojangUser.getId();
    }

    @Override
    public void onEnable()
    {
        instance = this;

        pluginManager = getProxy().getPluginManager();

        gson = new Gson();


        bannedPlayers = new Table("BannedPlayers",
                new VarChar("USERNAME", 100),
                new VarChar("BANNER", 100),
                new VarChar("UUID", 100),
                new VarChar("DURATION", 100),
                new VarChar("START", 100),
                new VarChar("END", 100),
                new VarChar("REASON", 100));

        banLog = new Table("BanLog",
                new VarChar("TYPE", 100),
                new VarChar("USERNAME", 100),
                new VarChar("BANNER", 100),
                new VarChar("UUID", 100),
                new VarChar("START", 100),
                new VarChar("DURATION", 100),
                new VarChar("REASON", 100)
        );
        try
        {
            File dataFolder = getDataFolder();
            if (!dataFolder.exists())
            {
                dataFolder.mkdir();
            }

            motdConfigFile = new File(getDataFolder(), "motd.json");
            templateConfigFile = new File(getDataFolder(), "templates.json");
            databaseConfigFile = new File(getDataFolder(), "database.json");
            rankConfigFile = new File(getDataFolder(), "ranks.json");
            userConfigFile = new File( getDataFolder(), "users.json" );

            motdConfig = ConfigLoader.load(motdConfigFile, MotdConfig.class);
            templateConfig = ConfigLoader.load(templateConfigFile, TemplateConfig.class);
            databaseConfig = ConfigLoader.load(databaseConfigFile, DatabaseConfig.class);
            rankConfig = ConfigLoader.load(rankConfigFile, RankConfig.class);
            userConfig = ConfigLoader.load( userConfigFile, UserConfig.class );

            if (motdConfig == null) motdConfig = new MotdConfig();
            if (templateConfig == null) templateConfig = new TemplateConfig();
            if (databaseConfig == null) databaseConfig = new DatabaseConfig();
            if (rankConfig == null) rankConfig = new RankConfig();


            database = new DatabaseDriver();
            database.connect();

            database.createTable(bannedPlayers);
            database.createTable(banLog);


            for (Command command : Arrays.asList(
                    new BuilderLocalChatCommand(),
                    new BuilderGlobalChatCommand(),
                    new TeamLeaderLocalChatCommand(),
                    new TeamLeaderGlobalChatCommand(),
                    new MoveCommand(),
                    new GoToCommand(),
                    new MotdCommand(),
                    new HubCommand(),
                    new CreateTemplateCommand(),
                    new DeleteTemplateCommand(),
                    new CheckTemplateCommand(),
                    new ListTemplateCommand(),
                    new BanCommand(), //TODO: BAN RANKING GlobalSystem.priorität.1, GlobalSystem.priorität.2
                    new TempBanCommand(),
                    new UnBanCommand(),
                    new CheckCommand(),
                    new BanLogCommand(),
                    new ClearBanlogCommand(),
                    new WarnCommand(),
                    new RemoveWarnCommand(),
                    new BanTemplateCommand(),
                    new KickCommand()
            ))
            {
                pluginManager.registerCommand(this, command);
            }

            for (Listener listener : Arrays.asList(
                    new ProxyPingListener(),
                    new ProxyConnectListener(),
                    new ProxyJoinListener()
            ))
            {
                pluginManager.registerListener(this, listener);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }


    }


    @Override
    public void onDisable()
    {

        try
        {
            ConfigLoader.save(motdConfigFile, motdConfig);
            ConfigLoader.save(templateConfigFile, templateConfig);
            ConfigLoader.save(rankConfigFile, rankConfig);
            ConfigLoader.save( userConfigFile, userConfig );

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        if (database != null) database.disconnect();
    }
}
