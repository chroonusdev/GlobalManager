import net.chroonus.ban.BanHandler;
import net.chroonus.util.TimeHelper;
import org.junit.Test;

import java.sql.Time;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TimeHelperTest
{
    @Test
    public void testCorrectString()
    {
        boolean valueable = TimeHelper.isValueable("3w7d18h6m3s");
        assertTrue(valueable);
    }

    @Test
    public void testIncorrectString()
    {
        boolean valuable = TimeHelper.isValueable("12a2342g345h");
        assertFalse(valuable);
    }

    @Test
    public void testUnitOverflow()
    {
        boolean valueable = TimeHelper.isValueable("60w12d30h100m70s");
        assertFalse(valueable);
    }

    @Test
    public void testSecondsToSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("1s");
        assertTrue(seconds == (long) 1);
    }

    @Test
    public void testMinutesToSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("2m");
        assertTrue(seconds == (long) 120);
    }

    @Test
    public void testHoursToSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("3h");
        assertTrue(seconds == (long) 10800);
    }

    @Test
    public void testDaysToSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("4d");
        assertTrue(seconds == (long) 345600);
    }

    @Test
    public void testWeeksToSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("5w");
        assertTrue(seconds == (long) 3024000);
    }

    @Test
    public void testCalculateSeconds()
    {
        long seconds = TimeHelper.calculateSeconds("3w7d18h6m3s");//1814400 + 604800 + 64800 + 360 + 3
        assertTrue(seconds == (long) 2484363);
    }
}
